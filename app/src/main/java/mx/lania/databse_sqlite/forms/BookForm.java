package mx.lania.databse_sqlite.forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.lania.databse_sqlite.MainActivity;
import mx.lania.databse_sqlite.R;
import mx.lania.databse_sqlite.db.models.Book;

public class BookForm extends AppCompatActivity {

    private EditText titleTxt;
    private EditText availTxt;
    private EditText priceTxt;
    private EditText authorTxt;
    private Button btnAddBookData;
    private Button btnCancel;

    private Book BookController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_form);

        //Enlazar componentes view
        titleTxt = (EditText) findViewById(R.id.titleTxt);
        availTxt = (EditText) findViewById(R.id.availTxt);
        priceTxt = (EditText) findViewById(R.id.priceTxt);
        authorTxt = (EditText) findViewById(R.id.authorTxt);
        btnAddBookData = (Button) findViewById(R.id.btnAddBookData);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        //Controlador del libro
        BookController = new Book(this);

        //Evanto del boton
        btnAddBookData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insertar libro
                BookController.openConnection();
                BookController.insertBook(titleTxt.getText().toString().trim(),
                        availTxt.getText().toString().trim(),
                        priceTxt.getText().toString().trim(),
                        authorTxt.getText().toString().trim());
                BookController.closeConnection();

                //Mostrar todos los libros
//                BookController.openConnection();
//                Cursor c = BookController.getBooks();
//                if (c.moveToFirst())
//                    do {
//                        displayBooks(c);
//                    } while (c.moveToNext());
//                BookController.closeConnection();

                titleTxt.setText("");
                availTxt.setText("");
                priceTxt.setText("");
                authorTxt.setText("");
                showMsg("Libro guardado correctamente");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(BookForm.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void displayBooks(Cursor c) {
        Toast.makeText(getBaseContext(),
                "ID: " + c.getString(0) + "\n"
                        + "Titulo: " + c.getString(1) + "\n"
                        + "Disponibilidad: " + c.getString(2) + "\n"
                        + "Precio: " + c.getString(3) + "\n"
                        + "Autor: " + c.getString(4) + "\n",
                Toast.LENGTH_LONG).show();
    }

    public void showMsg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}