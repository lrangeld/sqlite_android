package mx.lania.databse_sqlite.forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.lania.databse_sqlite.MainActivity;
import mx.lania.databse_sqlite.R;
import mx.lania.databse_sqlite.db.models.Member;

public class MemberForm extends AppCompatActivity {
    
    private EditText nameTxt;
    private EditText bdTxt;
    private EditText addressTxt;
    private EditText registryTxt;
    private Button btnAddMemberData;
    private Button btnCancel;
    
    private Member MemberController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_form);

        //Enlazar componentes view
        nameTxt = (EditText) findViewById(R.id.nameTxt);
        bdTxt = (EditText) findViewById(R.id.bdTxt);
        addressTxt = (EditText) findViewById(R.id.addressTxt);
        registryTxt = (EditText) findViewById(R.id.registryTxt);
        btnAddMemberData = (Button) findViewById(R.id.btnAddMemberData);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        //Controlador de Miembro
        MemberController = new Member(this);

        //Evanto del boton
        btnAddMemberData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insertar miembro
                MemberController.openConnection();
                MemberController.insertMember(nameTxt.getText().toString().trim(),
                        bdTxt.getText().toString().trim(),
                        addressTxt.getText().toString().trim(),
                        registryTxt.getText().toString().trim());
                MemberController.closeConnection();

                //Mostrar todos los miembros
//                MemberController.openConnection();
//                Cursor c = MemberController.getMembers();
//                if (c.moveToFirst())
//                    do {
//                        displayMembers(c);
//                    } while (c.moveToNext());
//                MemberController.closeConnection();

                nameTxt.setText("");
                bdTxt.setText("");
                addressTxt.setText("");
                registryTxt.setText("");
                showMsg("Miembro guardado correctamente");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MemberForm.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void displayMembers(Cursor c) {
        Toast.makeText(getBaseContext(),
                "ID: " + c.getString(0) + "\n"
                        + "Nombre: " + c.getString(1) + "\n"
                        + "Nacimiento: " + c.getString(2) + "\n"
                        + "Direccion: " + c.getString(3) + "\n"
                        + "Registro: " + c.getString(4) + "\n",
                Toast.LENGTH_LONG).show();
    }

    public void showMsg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}