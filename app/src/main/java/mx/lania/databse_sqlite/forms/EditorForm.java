package mx.lania.databse_sqlite.forms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import mx.lania.databse_sqlite.MainActivity;
import mx.lania.databse_sqlite.R;
import mx.lania.databse_sqlite.db.models.Editor;

public class EditorForm extends AppCompatActivity {

    private EditText nameEdTxt;
    private EditText addressEdTxt;
    private Button btnAddEditorData;
    private Button btnCancel;

    private Editor EditorController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor_form);

        //Enlazar componentes view
        nameEdTxt = (EditText) findViewById(R.id.nameEdTxt);
        addressEdTxt = (EditText) findViewById(R.id.addressEdTxt);
        btnAddEditorData = (Button) findViewById(R.id.btnAddEditorData);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        //Controlador de Miembro
        EditorController = new Editor(this);

        //Evanto del boton
        btnAddEditorData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Insertar miembro
                EditorController.openConnection();
                EditorController.insertEditor(nameEdTxt.getText().toString().trim(),
                        addressEdTxt.getText().toString().trim());
                EditorController.closeConnection();

                //Mostrar todos los miembros
//                EditorController.openConnection();
//                Cursor c = EditorController.getEditors();
//                if (c.moveToFirst())
//                    do {
//                        displayEditors(c);
//                    } while (c.moveToNext());
//                EditorController.closeConnection();

                nameEdTxt.setText("");
                addressEdTxt.setText("");
                showMsg("Editor guardado correctamente");
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EditorForm.this, MainActivity.class);
                startActivity(i);
            }
        });
    }

    public void displayEditors(Cursor c) {
        Toast.makeText(getBaseContext(),
                "ID: " + c.getString(0) + "\n"
                        + "Nombre: " + c.getString(1) + "\n"
                        + "Direccion: " + c.getString(2) + "\n",
                Toast.LENGTH_LONG).show();
    }

    public void showMsg(String msg){
        Toast.makeText(getBaseContext(), msg, Toast.LENGTH_SHORT).show();
    }
}